#define led_pin 3

bool led_pos = false;

void setup(){
  cli();
  TCCR1A = 0;
  TCCR1B = 0;
  
  TCCR1B = TCCR1B | (1 << CS12);

  TIMSK1 = TIMSK1 | (1 << TOIE1);
  pinMode(led_pin, OUTPUT);
  sei();
}

ISR(TIMER1_OVF_vect){
  led_pos = !led_pos;
}

void loop(){
  digitalWrite(led_pin, led_pos);
}

