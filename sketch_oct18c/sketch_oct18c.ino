#define led_pin 3

bool led_pos = false;
bool old_pos = false;

void setup(){
  Serial.begin(9600);
  cli();
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 62500;
  TCCR1B = TCCR1B | (1 << WGM12);

  TCCR1B = TCCR1B | (1 << CS12);

  TIMSK1 |= (1 << OCIE1A);
  pinMode(led_pin, OUTPUT);
  sei();
}

ISR(TIMER1_COMPA_vect){
  led_pos = !led_pos;
}

void loop(){
  digitalWrite(led_pin, led_pos);
  if (led_pos != old_pos){
    old_pos = led_pos;
    Serial.println(millis());
  }
}

