#define btn_pin 2
#define led_pin A2

bool state = LOW;
bool do_blink = true;

void setup() {
  cli();
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 62000;
  TCCR1B = TCCR1B | (1 << WGM12);

  TCCR1B = TCCR1B | (1 << CS12);

  TIMSK1 |= (1 << OCIE1A);
  pinMode(led_pin, OUTPUT);
  sei();
  pinMode(led_pin, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(btn_pin), _blink, RISING);
}

ISR(TIMER1_COMPA_vect){
  if (do_blink){
    state = !state;
  }
  
}

void loop() {
  digitalWrite(led_pin, state);
}


void _blink(){
  do_blink = !do_blink;
}